const utils = require('@rahul171/utils');
utils.configure({ len: 50 });

const Readable = require('stream').Readable;
const r = new Readable();

let i = 0;

r._read = function(n) {
    setTimeout(() => {
        const chunk = i >= 5 ? null : `read data ${++i}`;
        console.log(`r - ${chunk}`);
        r.push(chunk);
    }, 1000);
};

var Transform = require('stream').Transform;
var t = new Transform();
t._transform = function (chunk, encoding, next) {
    console.log(`t - ${chunk.toString()}`);
    next(null, chunk); // push chunk on to readable side
};

const Writable = require('stream').Writable;
const w = new Writable();

w._write = function(chunk, encoding, next) {
    console.log(`w - ${chunk.toString()}`);
    utils.line();
    next();
};

r.pipe(t).pipe(w);

