const a = (async () => {

    const debug = require('debug')('basics:index7');
    const mongoose = require('mongoose');

    mongoose.set('debug', true);

    try {
        await mongoose.connect('mongodb://localhost:27017', {
            useNewUrlParser: true,
            useUnifiedTopology: true
        });
    } catch (err) {
        debug(err);
    }

    const schema = new mongoose.Schema({
        name: { type: String },
        something: {
            a: { type: String },
            type: { type: String }
        },
        b: { type: Number }
    });

    const Person = mongoose.model('perso', schema);

    const p1 = new Person({
        name: 'rahul',
        something: {
            a: 'hello',
            type: 11
        }
    });

    // Person.find({}, (err, res) => {
    //     debug(err);
    //     debug(res);
    // });

    p1.save((err, res) => {
        if (err) {
            debug('err');
            debug(err);
        }
        debug('saved');
        debug(res);
    });

})();

a.then(data=>{console.log('data: ' + data);}).catch(err=>{
    console.log(err);
})

