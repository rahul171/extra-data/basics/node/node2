var Readable = require('stream').Readable;
var myStream = new Readable();

let i = 0;

myStream._read = function (n) {
    console.log(`myStream._read : ${Array.prototype.slice.call(arguments, 0).join(',')}`);
    setTimeout(() => {
        console.log(`myStream => before push - hello: ${i + 1}`);
        myStream.push(`hello: ${++i}`);
        if (i === 10) {
            console.log(`myStream => before push - END`);
            myStream.push(null);
        }
    });
};

var Writable = require('stream').Writable;
var w = new Writable();
w._write = function (chunk, encoding, next) {
    console.log(`w => write : ${chunk.toString()}`);
    next();
};

myStream.on('data', (chunk) => {
    console.log(`myStream => data : ${chunk.toString()}`);
    w.write(chunk);
})

myStream.on('end', () => {
    console.log('myStream => end');
});
