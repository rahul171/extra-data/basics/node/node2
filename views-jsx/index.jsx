const React = require('react');
const Header = require('./head');
const Body = require('./body');

function Default(props) {
    return (
        <html>
        <Header />
        <Body>
            <div className="main">
                <div>{props.message}</div>
            </div>
        </Body>
        </html>
    );
}

module.exports = Default;
