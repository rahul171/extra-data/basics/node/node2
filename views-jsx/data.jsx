const React = require('react');
const Header = require('./head');
const Body = require('./body');
const DataList = require('./datalist');

function Data(props) {
    return (
        <html>
        <Header />
        <Body>
            <DataList list={props.data} />
        </Body>
        </html>
    );
}

module.exports = Data;
