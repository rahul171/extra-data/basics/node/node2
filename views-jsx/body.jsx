const React = require('react');

function Body(props) {
    return (
        <body>
        {props.children}
        </body>
    );
}

module.exports = Body;
