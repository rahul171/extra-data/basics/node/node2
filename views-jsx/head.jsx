const React = require('react');

function Head(props) {
    return (
        <head lang="en">
            <title></title>
            <link rel="stylesheet" href="/static/style.css" />
            {props.children}
        </head>
    );
}

module.exports = Head;
