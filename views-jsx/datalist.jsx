const React = require('react');

function DataList(props) {
    let list = formatList(props.list);
    list = getRenderList(list);

    return (
        <div className="list">
            {list}
        </div>
    );
}

function getRenderList(list) {
    return list.map(item => {
        return (
            <div className="item">{item}</div>
        );
    });
}

function formatList(list) {
    if (typeof list === 'object') {
        return getArray(list);
    }
    return list;
}

function getArray(obj) {
    const out = [];
    for (const item in obj) {
        if (obj.hasOwnProperty(item)) {
            out.push(obj[item]);
        }
    }
    return out;
}

module.exports = DataList;
