const mysql = require('mysql');
const fs = require('fs');
const utils = require('@rahul171/utils');
const debug = require('debug')('basics:index6');

const con = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'abcd1234',
    database: 'sakila'
});

con.connect(function(err) {
    if (err) {
        throw err;
    }
    debug('connected');
});

setTimeout(() => {

    const Transform = require('stream').Transform;
    const t = new Transform();

    let i = 0;

    t._transform = (chunk, encoding, next) => {
        chunk = chunk.toString();
        chunk = (i++ === 0 ? '[' : ',') + chunk;
        next(null, chunk);
    };

    const writable = fs.createWriteStream('dbres.json');

    t.on('end', () => {
        writable.write(']');
    });

    t.pipe(writable);

    const query = con.query('select * from actor');

    query
        .on('error', (err) => {
            debug(err);
        })
        .on('fields', (fields) => {
            // debug('fields');
            // debug(fields);
        })
        .on('result', (row) => {
            debug('result');
            // debug(row);
            t.write(JSON.stringify(row));
        })
        .on('end', () => {
            debug('end');
            t.end();
        });

}, 1000);
