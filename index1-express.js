const express = require('express');
const app = express();
const debug = require('debug')('basics:index1');

const port = process.env.NODE_PORT || 3000;

// app.use('/static', (req, res, next) => {
//     console.log(`static: ${req.url}`);
//     next();
// });
//
// app.use((req, res, next) => {
//     console.log(req.url);
//     next();
// });
//
// app.use('/static', express.static(`${__dirname}/public`));

// app.get('/', (req, res) => {
//     console.log('me');
//     res.send('Hello there');
// });
//
// app.get('/json', (req, res) => {
//     res.json({a: 1, b: function(){console.log('hello');}, c:'hello'});
// })

let i = 0;

app.get('/something/:id?', (req, res, next) => {
    console.log('here 1');
    console.log(`hey - 1 - ${i++}`);
    next();
}, (req, res, next) => {
    console.log(`hey - 2 - ${i++}`);
    next();
}, (req, res, next) => {
    console.log(`hey - 3 - ${i++}`);
    res.send(`hey - ${i}`);
    console.log('heheHEHE');
    next();
}, (req, res, next) => {
    console.log(`hey - 4 - ${i++}`);
    // res.send(`hey - ${i}`);
    console.log('hehe');
    next();
}, (req, res, next) => {
    console.log(`hey - 5 - ${i++}`);
    next();
}, (err, req, res, next) => {
    console.log('4 length');
    console.log(err);
    console.log(`hey - 6 - ${i++}`);
    // next(err);
    next(err);
}, (req, res, next) => {
    console.log(`hey - 7 - ${i++}`);
    // console.log(err);
    next();
});

app.get('/something/:page?/:extra?', (req, res, next) => {
    console.log('another something');
    // res.send('really?');
    // console.log(err);
});

const aa = 1;

function a(aa) {
    // const aa = 5;
    const fn = arguments[1];
    fn();
    b();
    function b() {
        console.log(aa);
    }
}

a(33, function() {
    console.log(aa);
});

// app.get('/something/:id?/:page?', (req, res) => {
//     console.log('aa');
//     console.log('bb');
// })

app.listen(port, () => {
    debug(`listening on port ${port}`);
});

// debugger;
