const debug = require('debug')('basics:index3');
const express = require('express');
const app = express();

const through2 = require('through2');

app.set('views', `${__dirname}/views-jsx`);
app.set('view engine', 'jsx');
app.engine('jsx', require('express-react-views').createEngine());

app.use((req, res, next) => {
    debug(`${req.method} ${req.url}`);
    next();
});

app.use('/static', express.static('static'));

app.get('/', (req, res) => {
    res.render('index', {
        message: 'hello there!'
    });
});

app.get('/header/:id', (req, res) => {
    debug(req.params);
    res.json({
        body:req.body||'no data',
        qp: req.query,
        pm: req.params
    });
});

const fs = require('fs');

app.post('/header', (req, res) => {
    console.log(req.rawHeaders);
    const writable = fs.createWriteStream('abcd.txt');
    let total = 0;
    req.on('data', (chunk) => {
        console.log(chunk.toString().length);
        total += chunk.toString().length;
        writable.write(chunk);
        // console.log('total: ' + total);
    });
    req.on('end', () => {
        console.log('end');
        writable.close();
    });
    console.log('before end or after');
    res.send('something');
});

app.post('/something', jsonMiddleware, function(req, res, next) {
    console.log(req.body);
    res.send('adfd');
});

app.get('/data/:d1?/:d2?', (req, res) => {
    const data = { ...getDefaultData(), ...JSON.parse(JSON.stringify(req.params)) };
    res.render('data', { data });
});

app.listen(process.env.NODE_PORT || 3000, () => {
    debug('listening');
});

function getDefaultData() {
    return {
        d1: 0,
        d2: 0
    };
}

function jsonMiddleware(req, res, next) {
    if (req.header('content-type') !== 'application/json') {
        next();
    }
    let data = '';
    const Writable = require('stream').Writable;
    const w = new Writable();
    w._write = function (chunk, encoding, next) {
        data += chunk.toString();
        next();
    };
    req.pipe(w);
    req.on('end', () => {
        try {
            req.body = JSON.parse(data);
        } catch(err) {
            debug(err);
        }
        next();
    });
}
