const debug = require('debug')('basics:index2');
const express = require('express');
const app = express();

app.set('view engine', 'ejs');

app.use((req, res, next) => {
    debug(`${req.method} ${req.url}`);
    next();
});

app.use('/static', express.static('static'));

app.get('', (req, res) => {
    res.render('index');
});

app.get('/data/:d1?/:d2?', (req, res) => {
    req.params.abcd = null;
    const data = { ...{ d1: 0, d2: 0 }, ...JSON.parse(JSON.stringify(req.params)) };
    res.render('data', { data });
});

app.listen(process.env.NODE_PORT || 3000, () => {
    debug('listening');
});
